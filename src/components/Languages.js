import React from "react";
import { IntlContextConsumer, changeLocale } from "gatsby-plugin-intl";
import { Flag } from "semantic-ui-react";

const languageName = {
  en: "uk",
  pl: "pl",
  nl: "nl"
};

const Language = () => {
  return (
    <div style={{ display: 'flex' }}>
      <IntlContextConsumer>
        {({ languages, language: currentLocale }) =>
          languages.map(language => (
            <button
              key={languageName[language]}
              onClick={() => changeLocale(language)}
              style={{
                opacity: currentLocale === language ? 1 : .7,
                margin: 2,
                textDecoration: `none`,
                background: "none",
                cursor: `pointer`,
                boxShadow: `none`,
                border: 'none',

              }}
            >
              <Flag name={languageName[language]} />{" "}
            </button>
          ))
        }
      </IntlContextConsumer>
    </div>
  );
};

export default Language;
