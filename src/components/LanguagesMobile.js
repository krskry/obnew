import React from "react";
import { changeLocale, injectIntl, intlShape } from "gatsby-plugin-intl";
import { Dropdown } from "semantic-ui-react";

const propTypes = {
  intl: intlShape.isRequired
};

const Language = ({ intl }) => {
  return (
    <>
      <Dropdown style={{ color: "white" }} text={intl.locale.toUpperCase()}>
        <Dropdown.Menu>
          <Dropdown.Item role="button" text="EN" onClick={() => changeLocale("en")} />
          <Dropdown.Item role="button" text="PL" onClick={() => changeLocale("pl")} />
          <Dropdown.Item role="button" text="NL" onClick={() => changeLocale("nl")} />
        </Dropdown.Menu>
      </Dropdown>
    </>
  );
};

Language.propTypes = propTypes;

export default injectIntl(Language);
