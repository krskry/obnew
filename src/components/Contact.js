import React from "react";
import { Icon, Header } from "semantic-ui-react";
import parse from "html-react-parser";

export const Contact = (props) => {
  const translations = props.translations;
  return (
    <>
      <Header
        as="h2"
        style={{
          paddingTop: "10vh",
          fontSize: "2rem",
          color: "white",
          textAlign: "center",
          paddingBottom: "2rem",
        }}
      >
        {translations.contact_intro}
      </Header>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "start",
        }}
        className="contact"
      >
        <p
          style={{
            margin: 12,
            maxWidth: 700,
            fontSize: "1.33em",
            color: "white",
            lineHeight: "1.7em",
            textAlign: "center",
          }}
        >
          {parse(translations.contact_main)}
        </p>
        <h4 className="text-center" style={{ borderBottom: "1px solid white" }}>
          Rafal Lipinski
        </h4>
        <Icons />
      </div>
    </>
  );
};

export const Icons = () => {
  return (
    <div className="container">
      <div className=" ">
        <a
          style={{
            color: "white",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            margin: 12,
            fontSize: "1.2rem",
          }}
          href="mailto:rafal@offbeatmotion.tv"
        >
          <Icon
            style={{ paddingBottom: 24, marginRight: 16 }}
            name="envelope"
          />
          <>rafal@offbeatmotion.tv</>
        </a>
        <a
          style={{
            color: "white",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            margin: "1.5rem",
            fontSize: "1.2rem",
          }}
          href="tel:+31-626-973-849"
        >
          <Icon style={{ paddingBottom: 24, marginRight: 16 }} name="phone" />
          +31 626973849
        </a>
      </div>
    </div>
  );
};
