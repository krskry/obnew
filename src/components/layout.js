import React, { useEffect, useState } from "react";
import { Link, navigate } from "gatsby";
import Logo from "../../content/assets/logo.png";
import styled from "styled-components";
import { Footer } from "./footer";
import { Responsive } from "semantic-ui-react";
import { injectIntl, FormattedMessage } from "gatsby-plugin-intl";
import Languages from "./Languages";
import LanguagesMobile from "./LanguagesMobile";
import "semantic-ui-css/semantic.min.css";
import "../utils/wicked.scss";

const getWidth = () => {
  const isSSR = typeof window === "undefined";
  return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth;
};

const Navbar = styled.div`
  width: 100%;
  position: fixed;
  top: 0;
  left: 0;
  background-color: black;
  z-index: 100;
  display: flex;
  input + label {
    position: fixed;
    top: 25px;
    right: 0px;
    height: 20px;
    width: 68px;
    z-index: 12;
  }

  input + label span {
    position: absolute;
    width: 60%;
    height: 5px;
    top: 50%;
    margin-top: -1px;
    left: -2px;
    display: block;
    background: red;
    transition: 0.5s cubic-bezier(0.17, 0.37, 0.75, 1);
    border-radius: 2px;
  }

  input + label span:first-child {
    top: -7px;
  }

  input + label span:last-child {
    top: 26px;
  }

  label:hover {
    cursor: pointer;
  }

  input:checked + label span {
    opacity: 0;
    top: 50%;
  }

  input:checked + label span:first-child {
    opacity: 1;
    transform: rotate(405deg);
  }

  input:checked + label span:last-child {
    opacity: 1;
    transform: rotate(-405deg);
  }

  .menuListContainer {
    z-index: 90;
    height: 0px;
  }

  input {
    display: none;
    padding: 30px;
  }

  input ~ div {
    background: white;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    z-index: 1;
    transition: 0.5s cubic-bezier(0.17, 0.37, 0.75, 1);
    transition-delay: 0.5s;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  input ~ div > ul {
    padding: 0;
    align-items: center;
    justify-content: start;
    display: flex;
    flex-direction: column;
    list-style-type: none;
    margin: 0;
  }

  input ~ div > ul > li {
    opacity: 0;
    transition: 0.5s;
    transition-delay: 0s;
    width: 100%;
  }

  input ~ div > ul > li > a {
    text-decoration: none;
    text-transform: uppercase;
    color: black;
    font-weight: 300;
    font-size: 2em;
    display: block;
    padding: 0.4em;
    text-align: center;
    cursor: pointer;
  }

  input:checked ~ div {
    height: 100%;
    transition-delay: 0s;
  }

  input:checked ~ div > ul > li {
    opacity: 1;
    transition-delay: 0.5s;
  }

  .mobileNav {
    position: fixed;
    top: 0;
    right: 0;
    height: 68px;
    width: 68px;
    z-index: 99;
    cursor: pointer;
  }

  .mobileNav input + label {
    position: fixed;
    display: block;
    top: 25px;
    right: -14px;
    height: 20px;
    width: 64px;
    z-index: 99;
  }

  .mobileNav input + label span {
    position: absolute;
    width: 60%;
    height: 3px;
    top: 50%;
    margin-top: -1px;
    left: 0;
    display: block;
    background: white;
    transition: 0.5s;
    border: 1px solid white;
    border-radius: 10px;
  }
  .mobileNav input:checked + label span {
    background: black;
    border: 1px solid black;
  }
  .mobileNav input + label span:first-child {
    top: -7px;
  }

  .mobileNav input + label span:last-child {
    top: 26px;
  }

  .mobileNav label:hover {
    cursor: pointer;
  }

  .mobileNav input:checked + label span {
    opacity: 0;
    top: 50%;
  }

  .mobileNav input:checked + label span:first-child {
    opacity: 1;
    transform: rotate(405deg);
  }

  .mobileNav input:checked + label span:last-child {
    opacity: 1;
    transform: rotate(-405deg);
  }

  @media only screen and (max-width: 1023px) {
    opacity: 0.9;
    position: fixed;
    top: 0;
    right: 0;
    background-color: black;
    display: flex;
    height: 68px;
    justify-content: flex-end;
    align-items: center;
    z-index: 11;
    width: 20vw;
    min-width: 100vw;
    min-height: 68px;
    padding: 5px 68px 5px 12px;

    border: 1px solid black;

    .logo {
      width: 100vw;
      padding-top: 9px;
    }
    .logo .mainLogo {
      height: 50px;
      margin-right: 68px;
    }
  }
`;

const MdNav = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
  margin: auto;
  height: 58px !important;
  .mainLogoMd {
    margin-right: 1em;
    margin-top: 12px;
    cursor: pointer;
    height: 38px;
  }
  ul {
    list-style-type: none;
    transition: 1s;
    color: white;
    li {
      list-style-type: none;
      border-bottom: 1px solid transparent;
      border-radius: 1px;
      cursor: pointer;
      transition: hover 0.2s ease-out, border-bottom-color 0.2s ease-out;
      &:hover {
        border-bottom: 1px solid white;
        border-radius: 1px;
      }
      &.active {
        border-bottom: 1px solid white;
        border-radius: 1px;
      }
    }
  }
`;

const Navigation = (props) => {
  const location = props.location.href ? props.location.href : "";
  return (
    <Navbar>
      {getWidth() > 968 ? (
        <MdNav>
          <Link to={`/`}>
            <img src={Logo} alt="logo" className="mainLogoMd mr-5" />
          </Link>

          <ul
            style={{
              width: "680px",
              display: "flex",
              justifyContent: "space-around",
              alignItems: "center",
              height: "100%",
              color: "white",
            }}
          >
            <li
              style={{ marginLeft: 16, paddingTop: 16 }}
              className={location.includes("about") ? "active" : "empty"}
            >
              <Link
                style={{
                  boxShadow: `none`,
                  textDecoration: `none`,
                  color: `inherit`,
                  fontSize: 16,
                  textTransform: "uppercase",
                }}
                to={`/about`}
              >
                <FormattedMessage id="about" />
              </Link>
            </li>
            <li
              style={{ paddingTop: 16 }}
              className={location.includes("videos") ? "active" : "empty"}
            >
              <Link
                style={{
                  boxShadow: `none`,
                  textDecoration: `none`,
                  color: `inherit`,
                  fontSize: 16,
                  textTransform: "uppercase",
                }}
                to={`/videos/all`}
              >
                <FormattedMessage id="videos" />
              </Link>
            </li>
            <li
              style={{ paddingTop: 16 }}
              className={location.includes("photography") ? "active" : "empty"}
            >
              <Link
                style={{
                  boxShadow: `none`,
                  textDecoration: `none`,
                  color: `inherit`,
                  fontSize: 16,
                  textTransform: "uppercase",
                }}
                to={`/photography`}
              >
                <FormattedMessage id="photography" />
              </Link>
            </li>

            <li
              style={{ paddingTop: 16 }}
              className={location.includes("clients") ? "active" : "empty"}
            >
              <Link
                style={{
                  boxShadow: `none`,
                  textDecoration: `none`,
                  color: `inherit`,
                  fontSize: 16,
                  textTransform: "uppercase",
                }}
                to={`/clients`}
              >
                <FormattedMessage id="clients" />
              </Link>
            </li>
            <li
              style={{ paddingTop: 16 }}
              className={location.includes("contact") ? "active" : "empty"}
            >
              <Link
                style={{
                  boxShadow: `none`,
                  textDecoration: `none`,
                  color: `inherit`,
                  fontSize: 16,
                  textTransform: "uppercase",
                }}
                to={`/contact`}
              >
                <FormattedMessage id="contact" />
              </Link>
            </li>
            <li
              style={{
                marginLeft: "1em",
                paddingTop: 16,
                borderBottom: "none",
                width: 82,
              }}
            >
              <Languages />
            </li>
          </ul>
        </MdNav>
      ) : (
        <>
          <LanguagesMobile />
          <Link to={`/`} style={{ margin: "0 auto" }}>
            <img
              src={Logo}
              alt="logo"
              style={{ maxWidth: 160, margin: "auto" }}
              onClick={() => navigate("")}
            />
          </Link>
          <div
            className="mobileNav"
            onClick={(e) => {
              var burger = document.getElementById("burger");
              if (
                e.target === document.getElementsByClassName("mobileNav")[0]
              ) {
                if (burger.checked === false) {
                  burger.checked = true;
                } else {
                  burger.checked = false;
                }
              }
            }}
          >
            <input id="burger" type="checkbox" />
            <label htmlFor="burger">
              <span />
              <span />
              <span />
            </label>
            <div
              className="menuListContainer"
              onClick={(event) => event.preventDefault()}
            >
              <ul>
                <li>
                  <Link
                    style={{
                      boxShadow: `none`,
                      textDecoration: `none`,
                      color: `inherit`,
                    }}
                    to={`/`}
                  >
                    <FormattedMessage id="home" />
                  </Link>
                </li>
                <li>
                  <Link
                    style={{
                      boxShadow: `none`,
                      textDecoration: `none`,
                      color: `inherit`,
                      textTransform: "uppercase",
                    }}
                    to={`/videos/all`}
                  >
                    <FormattedMessage id="videos" />
                  </Link>
                </li>
                <li>
                  <Link
                    style={{
                      boxShadow: `none`,
                      textDecoration: `none`,
                      color: `inherit`,
                      textTransform: "uppercase",
                    }}
                    to={`/photography`}
                  >
                    <FormattedMessage id="photography" />
                  </Link>
                </li>
                <li>
                  <Link
                    style={{
                      boxShadow: `none`,
                      textDecoration: `none`,
                      color: `inherit`,
                      textTransform: "uppercase",
                    }}
                    to={`/about`}
                  >
                    <FormattedMessage id="about" />
                  </Link>
                </li>
                <li>
                  <Link
                    style={{
                      boxShadow: `none`,
                      textDecoration: `none`,
                      color: `inherit`,
                      textTransform: "uppercase",
                    }}
                    to={`/clients`}
                  >
                    <FormattedMessage id="clients" />
                  </Link>
                </li>
                <li>
                  <Link
                    style={{
                      boxShadow: `none`,
                      textDecoration: `none`,
                      color: `inherit`,
                      textTransform: "uppercase",
                    }}
                    to={`/contact`}
                  >
                    <FormattedMessage id="contact" />
                  </Link>
                </li>
                {/* <li style={{ borderBottom: "none !important" }}></li> */}
              </ul>
            </div>
          </div>
        </>
      )}
    </Navbar>
  );
};

const Layout = (props) => {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    setLoaded(true);
  }, []);

  const { location, children } = props;

  return (
    <LayoutCss style={{ background: "black", minHeight: "100vh" }}>
      {loaded && (
        <>
          <Navigation location={location} />
          <main>{children}</main>
          <Footer />
        </>
      )}
    </LayoutCss>
  );
};

const LayoutCss = styled.div``;

export default injectIntl(Layout);
