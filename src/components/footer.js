import React from "react";
import Vimeo from "../../content/assets/vimeo.png";
import FB from "../../content/assets/fb.png";
import YT from "../../content/assets/yt.png";
import Insta from "../../content/assets/insta.png";
import { Link } from "gatsby";

export const Footer = (props) => {
  return (
    <footer className={"footer"} id="footer">
      <div style={{ backgroundColor: "black" }}>
        <a
          style={{ borderBottom: "none" }}
          href="https://www.facebook.com/Offbeat-Motion-1878745515727983/"
          target="_blank"
          rel="noopener noreferrer"
        >
          <img src={FB} alt="vimeo" />
        </a>
        <a
          style={{ borderBottom: "none" }}
          href="https://vimeo.com/offbeatmotion"
          target="_blank"
          rel="noopener noreferrer"
        >
          <img src={Vimeo} alt="vimeo" />
        </a>
        <a
          style={{ borderBottom: "none" }}
          href="https://www.youtube.com/channel/UCc5xYdSn1EX96F1cjuGk45Q"
          target="_blank"
          rel="noopener noreferrer"
        >
          <img src={YT} alt="yt" />
        </a>
        <a
          style={{ borderBottom: "none" }}
          href="https://www.instagram.com/offbeat_motion/"
          target="_blank"
          rel="noopener noreferrer"
        >
          <img src={Insta} alt="insta" />
        </a>
      </div>
      <p
        style={{
          display: "flex",
          justifyContent: "center",
          paddingBottom: ".2rem",
          color: "rgba(255,255,255,0.7)",
          letterSpacing: 1.22,
        }}
      >
        &copy; {new Date().getFullYear()}
        <Link
          style={{ marginLeft: 4, color: "rgba(255,255,255,0.7)" }}
          to={`/`}
        >
          Offbeat Motion
        </Link>
      </p>
      <p
        style={{
          display: "flex",
          justifyContent: "center",
          paddingBottom: "1rem",
          color: "rgba(255,255,255,0.6)",
          letterSpacing: 1.22,
        }}
      >
        KVK: 81464517
      </p>
    </footer>
  );
};
