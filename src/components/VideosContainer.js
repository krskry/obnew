import React from "react";
import Img from "gatsby-image";
const VideoContainer = props => {
  let item = props;
  return (
    <>
      <div
        className="miniVideoConatainer"
        onClick={e => props.openModal(e, item.slug)}
      >
        <Img className="miniVideo" fluid={props.photo.childImageSharp.fluid} />
        <div className="alt" id={`mini-text-${item.date}`}>
          <p
            style={{
              lineHeight: '1.4em',
              fontSize: '1.3em',
              background: 'rgba(0,0,0,0.5)',
              position: "absolute",
              padding: '.4rem',
              width: "100%",
              bottom: '0',
              textTransform: "uppercase"
            }}
          >
            {item.title}
          </p>
        </div>
      </div>
    </>
  );
};
export default VideoContainer;
