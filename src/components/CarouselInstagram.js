import React from "react";
import { Responsive } from "semantic-ui-react";
import Img from "gatsby-image";
import { useStaticQuery } from "gatsby";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";

const CarouselInstagram = (props) => {
  const data = useStaticQuery(graphql`
    query {
      instabig: file(relativePath: { eq: "instabig.png" }) {
        childImageSharp {
          fluid(quality: 90, maxWidth: 240) {
            ...GatsbyImageSharpFluid_withWebp_noBase64
          }
        }
      }
    }
  `);
  const [images, setImages] = React.useState([]);
  const [loaded, setLoaded] = React.useState(false);
  const clearStorage = () => {
    const lastclear = localStorage.getItem("lastclear"),
      time_now = new Date().getTime();
    if (time_now - lastclear > 1000 * 60 * 60 * 1) {
      localStorage.clear();
      localStorage.setItem("lastclear", time_now);
    }
  };

  const settings = {
    dots: true,
    infinite: true,
    speed: 2000,
    slidesToShow: 7,
    slidesToScroll: 1,
    swipeToSlide: true,
  };

  const settingsMobile = {
    dots: true,
    infinite: true,
    speed: 2000,
    slidesToShow: 3,
    slidesToScroll: 1,
    swipeToSlide: true,
  };

  async function getInstagramPictures(profileName) {
    const baseUrl = "https://www.instagram.com";
    const profileUrl = `${baseUrl}/${profileName}`;
    const jsonDataUrl = `${profileUrl}/?__a=1`;

    const response = await fetch(jsonDataUrl);
    const jsonData = await response.json();
    const pictures = jsonData.graphql.user.edge_owner_to_timeline_media.edges;

    if (response.ok) {
      return pictures;
    } else {
      throw new Error(pictures);
    }
  }
  const fetchPhotos = async () => {
    let instaFeed = localStorage.getItem("instaFeed")
      ? localStorage.getItem("instaFeed")
      : false;
    if (!instaFeed) {
      getInstagramPictures("offbeat_motion")
        .then((pictures) => {
          localStorage.setItem("instaFeed", JSON.stringify(pictures));
          setImages(pictures);
          setLoaded(true);
          console.log("Pictures:", pictures);
        })
        .catch((error) => console.error("Error:", error));
    } else {
      setImages(JSON.parse(instaFeed));
      setLoaded(true);
    }
  };

  React.useEffect(() => {
    clearStorage();
    fetchPhotos();
  }, []);

  return (
    <div
      style={{
        opacity: props.visible && loaded ? 1 : 0,
        transition: "opacity 1s ease",
      }}
    >
      <Img
        className="img-fluid instabig mt-4 mb-4"
        fluid={data.instabig.childImageSharp.fluid}
        alt="instagram"
      />

      {loaded && (
        <div className="carouselContainer d-flex align-items-center  flex-column">
          <Responsive minWidth={700}>
            {loaded && (
              <Slider {...settings}>
                {loaded &&
                  images.map((item, index) => (
                    <a
                      key={`insta-${index}`}
                      href={"https://www.instagram.com/offbeat_motion/"}
                      target="_blank"
                      rel="noopener noreferrer"
                      style={{
                        height: "calc(100vw / 7)",
                        width: "calc(100vw / 7)",
                      }}
                    >
                      <div
                        style={{
                          backgroundImage: `url(${item.node.display_url})`,
                        }}
                        className="instaImgDiv d-flex justify-content-start align-items-start"
                      />
                    </a>
                  ))}
              </Slider>
            )}
          </Responsive>
          <Responsive maxWidth={700}>
            <Slider {...settingsMobile}>
              {loaded &&
                images.map((item) => (
                  <a
                    key={`insta-${item.name}`}
                    href={item.link}
                    rel="noopener noreferrer"
                    target="_blank"
                  >
                    <div
                      style={{
                        height: "calc(100vw / 3)",
                        width: "calc(100vw / 3)",
                        backgroundSize: "cover",
                        backgroundImage: `url(${item.node.display_url})`,
                      }}
                      className="instaImgDiv d-flex justify-content-start align-items-start"
                    />
                  </a>
                ))}
            </Slider>
          </Responsive>
        </div>
      )}
    </div>
  );
};

export default CarouselInstagram;
