import React from "react";
import { graphql } from "gatsby";
import Helmet from "react-helmet";
import favicon from "../../content/assets/website-icon.ico";
import { Segment } from "semantic-ui-react";

import Layout from "../components/layout";
import SEO from "../components/seo";
import { Contact } from "../components/Contact";

const Page = props => {
  const { data } = props;
  const siteTitle = data.site.siteMetadata.title;
  const translations = data.translations.frontmatter;

  return (
    <Layout location={props.location} title={siteTitle}>
      <Helmet
        title="Contact"
        link={[{ rel: "shortcut icon", type: "image/png", href: `${favicon}` }]}
      />
      <SEO
        title={translations.seo_title_contact}
        description={translations.seo_desc_contact}
      />

      <Segment
        style={{ padding: "4em 0em", minHeight: "calc(100vh - 144px)" }}
        vertical
      >
        <Contact translations={translations} loction={props.location} />
      </Segment>
    </Layout>
  );
};

export default Page;

export const pageQuery = graphql`
  query($locale: String) {
    translations: markdownRemark(frontmatter: { locale: { eq: $locale } }) {
      frontmatter {
        seo_title_contact
        seo_desc_contact
        contact_intro
        contact_main
      }
    }
    site {
      siteMetadata {
        title
      }
    }
  }
`;
