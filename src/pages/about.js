import React from "react";
import { graphql } from "gatsby";
import Layout from "../components/layout";
import SEO from "../components/seo";
import Helmet from "react-helmet";
import favicon from "../../content/assets/website-icon.ico";
import { Grid, Segment, Header, Responsive } from "semantic-ui-react";

import Img from "gatsby-image";

const Page = (props) => {
  const { data } = props;
  const siteTitle = data.site.siteMetadata.title;
  const translations = data.translations.frontmatter;

  return (
    <Layout location={props.location} title={siteTitle}>
      <SEO
        title={translations.seo_title_about}
        description={translations.seo_desc_about}
      />
      <Helmet
        title="Offbeat Motion"
        link={[{ rel: "shortcut icon", type: "image/png", href: `${favicon}` }]}
      />

      <Segment
        style={{ padding: "8em 0em 4em 0", minHeight: "calc(100vh - 144px)" }}
        vertical
      >
        <Grid container stackable verticalAlign="middle">
          <Grid.Row>
            <Grid.Column width={8}>
              <Header as="h3" style={{ fontSize: "2em", color: "white" }}>
                {translations.about_intro}
              </Header>
              <Responsive maxWidth={700}>
                <Img
                  style={{ margin: "1.5rem 0" }}
                  fluid={
                    data.allMarkdownRemark.edges[0].node.frontmatter
                      .about_photo_1.childImageSharp.fluid
                  }
                />
              </Responsive>
              <p
                style={{
                  fontSize: "1.33em",
                  color: "white",
                  lineHeight: "1.7em",
                }}
              >
                {translations.about_main}
              </p>
              <Responsive maxWidth={700}>
                <Img
                  style={{ margin: "1.5rem 0" }}
                  fluid={
                    data.allMarkdownRemark.edges[0].node.frontmatter
                      .about_photo_2.childImageSharp.fluid
                  }
                />{" "}
              </Responsive>
              <Header as="h3" style={{ fontSize: "2em", color: "white" }}>
                {translations.about_final}
              </Header>
            </Grid.Column>
            <Grid.Column floated="right" width={6}>
              <Responsive minWidth={700}>
                <Img
                  style={{ margin: "1.5rem 0" }}
                  fluid={
                    data.allMarkdownRemark.edges[0].node.frontmatter
                      .about_photo_1.childImageSharp.fluid
                  }
                />
                <Img
                  style={{ margin: "1.5rem 0" }}
                  fluid={
                    data.allMarkdownRemark.edges[0].node.frontmatter
                      .about_photo_2.childImageSharp.fluid
                  }
                />
              </Responsive>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Segment>
    </Layout>
  );
};

export default Page;
export const fluidImage = graphql`
  fragment fluidImage on File {
    childImageSharp {
      fluid(maxWidth: 300, quality: 95) {
        ...GatsbyImageSharpFluid_withWebp
      }
    }
  }
`;

export const pageQuery = graphql`
  query($locale: String) {
    site {
      siteMetadata {
        title
      }
    }
    translations: markdownRemark(frontmatter: { locale: { eq: $locale } }) {
      frontmatter {
        seo_title_about
        seo_desc_about
        about_intro
        about_final
        about_main
      }
    }
    AboutPhoto1: file(relativePath: { eq: "1-min.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 300, quality: 95) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }

    AboutPhoto2: file(relativePath: { eq: "2-min.jpg" }) {
      childImageSharp {
        fluid(maxWidth: 300, quality: 95) {
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
    allMarkdownRemark(
      sort: { fields: [frontmatter___order], order: ASC }
      filter: { fileAbsolutePath: { regex: "/(about)/" } }
    ) {
      edges {
        node {
          excerpt
          fields {
            slug
          }
          frontmatter {
            about_photo_1 {
              childImageSharp {
                fluid(maxWidth: 300, quality: 95) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
            about_photo_2 {
              childImageSharp {
                fluid(maxWidth: 300, quality: 95) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
          }
        }
      }
    }
  }
`;
