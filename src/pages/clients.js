import React from "react";
import { graphql } from "gatsby";
import Helmet from "react-helmet";
import favicon from "../../content/assets/website-icon.ico";
import { Grid, Segment, Header } from "semantic-ui-react";
import Img from "gatsby-image";

import Layout from "../components/layout";
import SEO from "../components/seo";
import styled from "styled-components";

const ImageBox = styled.a`
  text-align: center;
  img {
    opacity: 0.95;
    &:hover {
      opacity: 1;
    }
  }
`;

const Page = (props) => {
  const { data } = props;
  const siteTitle = data.site.siteMetadata.title;
  const clients = data.allMarkdownRemark.edges;
  const translations = data.translations.frontmatter;
  return (
    <Layout location={props.location} title={siteTitle}>
      <SEO
        title={translations.seo_title_clients}
        description={translations.seo_description_clients}
      />
      <Helmet
        title="Offbeat Motion"
        link={[{ rel: "shortcut icon", type: "image/png", href: `${favicon}` }]}
      />

      <Segment
        style={{
          padding: "8em 0em 4em 0em",
          minHeight: "calc(100vh - 144px)",
        }}
        vertical
      >
        <div>
          <Header
            as="h3"
            style={{ fontSize: "2em", color: "white", textAlign: "center" }}
          >
            {translations.trusted_by}
          </Header>
          <div className="images d-none d-md-flex flex-column flex-md-row  align-items-center justify-content-center"></div>
          <div
            style={{
              color: "white",
              margin: "5em auto",
              maxWidth: 1200,
            }}
          >
            <Grid doubling stackable columns={4}>
              {clients.map(({ node }) => {
                // const title = node.frontmatter.title || node.fields.slug;
                const slug = "http://" + node.frontmatter.link;
                return (
                  <Grid.Column className="noPadding">
                    <ImageBox
                      href={slug}
                      target="_blank"
                      rel="noopener norefferer"
                    >
                      <Img
                        style={{
                          maxWidth: 180,
                          margin: "14px auto",
                        }}
                        key={node.slug}
                        fluid={node.frontmatter.photo.childImageSharp.fluid}
                        alt={node.frontmatter.title}
                      />
                    </ImageBox>
                  </Grid.Column>
                );
              })}
            </Grid>
          </div>
        </div>
      </Segment>
    </Layout>
  );
};

export default Page;

export const pageQuery = graphql`
  query($locale: String) {
    site {
      siteMetadata {
        title
      }
    }
    translations: markdownRemark(frontmatter: { locale: { eq: $locale } }) {
      frontmatter {
        seo_title_clients
        seo_desc_clients
        trusted_by
        contact_main
      }
    }
    allMarkdownRemark(
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { fileAbsolutePath: { regex: "/(clients)/" } }
    ) {
      edges {
        node {
          excerpt
          frontmatter {
            title
            link
            photo {
              childImageSharp {
                # Specify a fluid image and fragment
                # The default maxWidth is 800 pixels
                fluid(quality: 95) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
          }
        }
      }
    }
  }
`;
