import React from "react";
import { graphql } from "gatsby";
import { Header } from "semantic-ui-react";
import Layout from "../components/layout";
import SEO from "../components/seo";
import Helmet from "react-helmet";
import favicon from "../../content/assets/website-icon.ico";
import styled from "styled-components";
import Img from "gatsby-image";
const BigImage = styled.div``;
const ImageBox = styled.a`
  @media only screen and (min-width: 900px) {
    .masonry {
      margin-left: 5%;
      display: flex;
      flex-flow: row wrap;
      width: 100%;
    }
    .item {
      margin: 0 4px 8px 4px;
      overflow: hidden;
    }
    .item,
    .item-image {
      height: 480px;
    }

    .item-image:nth-child(1),
    .item:nth-child(1),
    .item-image:nth-child(6),
    .item:nth-child(6),
    .item-image:nth-child(9),
    .item:nth-child(9),
    .item-image:nth-child(13),
    .item:nth-child(13),
    .item-image:nth-child(18),
    .item:nth-child(18),
    .item-image:nth-child(21),
    .item:nth-child(21) {
      min-width: calc(30% - 8px);
    }

    .item-image:nth-child(2),
    .item:nth-child(2),
    .item-image:nth-child(8),
    .item:nth-child(8),
    .item-image:nth-child(11),
    .item:nth-child(11),
    .item-image:nth-child(14),
    .item:nth-child(14),
    .item-image:nth-child(6),
    .item:nth-child(20),
    .item-image:nth-child(23),
    .item:nth-child(23) {
      min-width: calc(30% - 8px);
    }
    .item-image:nth-child(3),
    .item:nth-child(3),
    .item-image:nth-child(4),
    .item:nth-child(4),
    .item-image:nth-child(5),
    .item:nth-child(5),
    .item-image:nth-child(7),
    .item:nth-child(7),
    .item-image:nth-child(10),
    .item:nth-child(10),
    .item-image:nth-child(12),
    .item:nth-child(12),
    .item-image:nth-child(15),
    .item:nth-child(15),
    .item-image:nth-child(16),
    .item:nth-child(16),
    .item-image:nth-child(17),
    .item:nth-child(17),
    .item-image:nth-child(19),
    .item:nth-child(19),
    .item-image:nth-child(22),
    .item:nth-child(22),
    .item-image:nth-child(24),
    .item:nth-child(24) {
      min-width: calc(15% - 8px);
    }
  }
  img {
    opacity: 0.95;
    &:hover {
      opacity: 1;
    }
  }
`;

const Page = (props) => {
  const { data } = props;
  const [modalOpen, setModalOpen] = React.useState(false);
  const [photo, setPhoto] = React.useState("");

  const siteTitle = data.site.siteMetadata.title;
  const photoData = data.allMarkdownRemark.edges;
  const translations = data.translations.frontmatter;
  const photos = photoData.map(({ node }) => {
    const width = node.frontmatter.width;
    const height = node.frontmatter.height;
    return { src: node.frontmatter.photo, width, height };
  });

  const openModal = (phot) => {
    console.log("sc");
    setPhoto(phot);
    setModalOpen(true);
  };

  return (
    <Layout location={props.location} title={siteTitle}>
      <Helmet
        title="Offbeat Motion"
        link={[{ rel: "shortcut icon", type: "image/png", href: `${favicon}` }]}
      />
      <SEO
        title={translations.seo_title_photography}
        description={translations.seo_desc_photography}
      />

      <div
        style={{
          minHeight: "calc(100vh - 144px)",
          padding: "8rem 6px 0 14px",
          display: "flex",
          flexDirection: "column",
          width: "100%",
        }}
      >
        <Header
          as="h3"
          style={{
            fontSize: "2em",
            color: "white",
            textAlign: "center",
            paddingBottom: "2rem",
          }}
          children={translations.photography_header}
        />

        {photos.length > 0 && (
          <ImageBox>
            <div className="masonry">
              {photos.map((phot, id) => {
                return (
                  <div
                    className="item"
                    key={id}
                    onClick={() => openModal(phot)}
                  >
                    <Img
                      className="item-image"
                      fluid={phot.src.childImageSharp.fluid}
                    />{" "}
                  </div>
                );
              })}
            </div>
          </ImageBox>
        )}
        {modalOpen && (
          <div id="myModal" className="modal">
            <div
              className="close"
              onClick={() => {
                setPhoto("");
                setModalOpen(false);
              }}
            >
              &times;
            </div>
            <div className="modal-content" id="modal-content" />
            <div
              style={{ display: "flex" }}
              className="vimeoMovieContainer fadeIn"
            >
              {photo === "" ? (
                ""
              ) : (
                <BigImage>
                  <Img
                    style={{ maxWidth: 800, width: "90vw", maxHeight: "100vh" }}
                    fluid={photo.src.childImageSharp.fluid}
                  />
                </BigImage>
              )}
            </div>
          </div>
        )}
      </div>
    </Layout>
  );
};

export default Page;

export const pageQuery = graphql`
  query($locale: String) {
    site {
      siteMetadata {
        title
      }
    }
    translations: markdownRemark(frontmatter: { locale: { eq: $locale } }) {
      frontmatter {
        photography_header
        seo_title_photography
        seo_desc_photography
      }
    }
    allMarkdownRemark(
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { fileAbsolutePath: { regex: "/(photos)/" } }
    ) {
      edges {
        node {
          excerpt
          frontmatter {
            photo {
              childImageSharp {
                # Specify a fluid image and fragment
                # The default maxWidth is 600 pixels
                fluid(maxWidth: 400, quality: 95) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
          }
        }
      }
    }
  }
`;
