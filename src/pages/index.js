import React, { useState, useEffect, useRef } from "react";
import { graphql } from "gatsby";
import styled from "styled-components";
import Layout from "../components/layout";
import SEO from "../components/seo";
import { navigate } from "gatsby";
import ArrowDown from "../../content/assets/arrowDown.svg";
import VideoContainer from "../components/VideosContainer";
import CarouselInstagram from "../components/CarouselInstagram";
import Helmet from "react-helmet";
import favicon from "../../content/assets/website-icon.ico";
import { Visibility, Grid } from "semantic-ui-react";
import Logo from "../../content/assets/logo.png";

const MainVideoCotainer = styled.div`
  width: 100vw;
  height: 100vh;
  overflow: hidden;
  position: relative;
  .vimeo-wrapper {
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: -1;
    pointer-events: none;
    overflow: hidden;
  }
  .vimeo-wrapper iframe {
    /* width: 100vw; */
    /* Given a 16:9 aspect ratio, 9/16*100 = 56.25 */
    width: 100vw;
    height: 100vh; /* Given a 16:9 aspect ratio, 9/16*100 = 56.25 */

    min-height: 100vh;
    min-width: 177.77vh; /* Given a 16:9 aspect ratio, 16/9*100 = 177.77 */

    /* min-width: 120vw; */
    @media only screen and (min-width: 900px) {
      min-height: 100vw;
      min-width: 120vw;
    }
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;
const MainVideouttonsContainer = styled.div`
  height: auto;
  position: absolute;
  top: 55%;
  left: 0;
  text-align: center;
  cursor: pointer;
  width: 3%;
  padding: 5%;
  display: flex;
  flex-wrap: wrap;
  width: 100vw;
  z-index: 99;
`;

const CoverBtn = styled.div`
  margin: 1em;
  color: white;
  background-color: transparent;
  border: 2px solid white;
  border-radius: 50px;
  padding: 15px 25px;
  cursor: pointer;
  height: 60px;
  text-transform: capitalize;
  line-height: 26px;
  &:hover {
    background: rgba(255, 255, 255, 0.3);
  }
`;

const CoverBox = styled.div`
  overflow: hidden;
  .cover {
    height: auto;
    box-sizing: border-box;
    background-color: black;
    z-index: 3;
    position: relative;
    display: flex;
    justify-content: center;
    .arrow {
      margin: 1em;
      color: white;
    }
  }
`;

const BlogIndex = (props) => {
  const [loaded, setLoaded] = useState(false);
  const [hide, setHide] = useState(false);
  const refContainer = useRef(null);

  const [instaVisible, setInstaVisible] = useState(false);
  const [embed, setEmbed] = useState("");
  const [modalOpen, setModalOpen] = useState(false);
  const { data } = props;
  const siteTitle = data.site.siteMetadata.title;
  const translations = data.translations.frontmatter;
  const posts = data.allMarkdownRemark.edges;

  const openModal = (e, src) => {
    e.preventDefault();
    setEmbed(src);
    setModalOpen(true);
  };

  const scrollTo = () => {
    if (refContainer.current) {
      const element = refContainer.current;
      const elemPos = element
        ? element.getBoundingClientRect().top + window.pageYOffset
        : 0;
      console.log(elemPos);

      setTimeout(() => {
        window.scroll({ top: elemPos - 50, left: 0, behavior: "smooth" });
      }, 0);
    }
  };

  useEffect(() => {
    loaded && setTimeout(() => setHide(true), 4000);
  }, [loaded]);

  const ReturnMainVideoStart = () => {
    return (
      <CoverBox>
        <section id="cover" className="cover">
          <MainVideoCotainer>
            <div
              className="vimeo-wrapper"
              style={{ opacity: hide ? 1 : 0, transition: "ease 4s " }}
            >
              {typeof document !== "undefined" && (
                <iframe
                  src="https://player.vimeo.com/video/503841360?background=1&autoplay=1&loop=1&byline=0&title=0"
                  frameBorder="0"
                  webkitallowfullscreen="true"
                  mozallowfullscreen="true"
                  allowFullScreen
                  title="hero video"
                  onLoad={() => setLoaded(true)}
                ></iframe>
              )}
            </div>
            {!hide && (
              <div
                style={{
                  zIndex: 99,
                  position: "absolute",
                  top: "calc(50vh - 25px)",
                  left: "calc(50vw - 115px)",
                  fontSize: 99,
                  color: "white",
                  transform: "scale(1.3)",
                  opacity: !loaded ? 0.8 : 0,
                  transition: "ease 3s ",
                }}
              >
                <img src={Logo} alt="" />
              </div>
            )}
          </MainVideoCotainer>

          <MainVideouttonsContainer>
            <CoverBtn
              style={{ textTransform: "uppercase" }}
              onClick={(e) => openModal(e, "502575772")}
            >
              {translations.showreel}
            </CoverBtn>
            <CoverBtn
              style={{ textTransform: "uppercase" }}
              onClick={() => navigate("contact")}
            >
              {translations.contact}
            </CoverBtn>

            <span
              className="pulse"
              style={{ display: "flex", alignItems: "center" }}
              onClick={() => scrollTo()}
            >
              <img
                src={ArrowDown}
                style={{ height: 60, margin: "1em" }}
                alt="arrow"
              />
              <i className="fa fa-arrow-circle-down  fa-4x pulse" />
            </span>
          </MainVideouttonsContainer>
        </section>
      </CoverBox>
    );
  };

  return (
    <Layout location={props.location} title={siteTitle}>
      <SEO
        title={translations.seo_title_index}
        description={translations.seo_desc_index}
      />
      <Helmet
        title="Offbeat Motion"
        link={[{ rel: "shortcut icon", type: "image/png", href: `${favicon}` }]}
      />
      {ReturnMainVideoStart()}
      <div id="masonry" ref={refContainer}>
        <Grid stackable doubling columns={3} className="videoList">
          {posts.map(({ node }) => {
            return (
              <Grid.Column key={node.frontmatter.embed} className="noPadding">
                <VideoContainer
                  openModal={(e, src) => openModal(e, src)}
                  key={node.frontmatter.title}
                  slug={node.frontmatter.embed}
                  title={node.frontmatter.title}
                  photo={node.frontmatter.photo}
                />
              </Grid.Column>
            );
          })}
        </Grid>
      </div>
      <Visibility onOnScreen={() => setInstaVisible(true)} />
      <CarouselInstagram visible={instaVisible} />
      {modalOpen && (
        <div id="myModal" className="modal">
          <div
            className="close"
            onClick={() => {
              setEmbed("");
              setModalOpen(false);
            }}
          >
            &times;
          </div>
          <div className="modal-content" id="modal-content" />
          <div className="vimeoMovieContainer fadeIn">
            {embed === "" ? (
              ""
            ) : (
              <iframe
                title={embed}
                id="vimeoMoviePlayed"
                className="vimeoMovie"
                src={`https://player.vimeo.com/video/${embed}?title=0&byline=0&portrait=0`}
                frameBorder="0"
                webkitallowfullscreen="true"
                mozallowfullscreen="true"
                allowFullScreen
              ></iframe>
            )}
          </div>
        </div>
      )}
    </Layout>
  );
};

export default BlogIndex;

export const pageQuery = graphql`
  query($locale: String) {
    site {
      siteMetadata {
        title
      }
    }

    translations: markdownRemark(frontmatter: { locale: { eq: $locale } }) {
      frontmatter {
        showreel
        contact
        seo_title_index
        seo_desc_index
      }
    }
    allMarkdownRemark(
      sort: { fields: [frontmatter___order], order: ASC }
      filter: { fileAbsolutePath: { regex: "/(movies)/" } }
    ) {
      edges {
        node {
          excerpt
          fields {
            slug
          }
          frontmatter {
            date
            title
            embed
            photo {
              childImageSharp {
                # Specify a fluid image and fragment
                # The default maxWidth is 800 pixels
                fluid(quality: 95, maxWidth: 600) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
            order
          }
        }
      }
    }
  }
`;
