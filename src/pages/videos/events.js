import React, { useState } from "react";
import { graphql } from "gatsby";
import Helmet from "react-helmet";
import favicon from "../../../content/assets/website-icon.ico";
import VideoContainer from "../../components/VideosContainer";
import { Menu, Grid } from "semantic-ui-react";
import { navigate } from "gatsby";
import Layout from "../../components/layout";
import SEO from "../../components/seo";
import { injectIntl } from "gatsby-plugin-intl";

const Page = (props) => {
  const [embed, setEmbed] = useState("");

  const openModal = (e, src) => {
    e.preventDefault();
    setEmbed(src);

    if (document.getElementById("myModal")) {
      document.getElementById("myModal").style.display = "block";
    }
  };
  const { data } = props;
  const siteTitle = data.site.siteMetadata.title;
  let posts = data.allMarkdownRemark.edges;
  const location = props.location.href ? props.location.href : "";
  posts = posts.filter((x) => x.node.frontmatter.category === "EVENT");
  const intl = props.intl;

  return (
    <Layout location={props.location} title={siteTitle}>
      <SEO title="Events Videos" />
      <Helmet
        title="Offbeat Motion"
        link={[{ rel: "shortcut icon", type: "image/png", href: `${favicon}` }]}
      />
      <div
        style={{
          minHeight: "calc(100vh - 144px)",
          padding: "8em 0 4em 0",
        }}
      >
        <Menu inverted stackable fluid widths={5}>
          <Menu.Item
            name={intl.formatMessage({ id: "all" })}
            active={location.includes("videos/all")}
            onClick={() => navigate("/videos/all")}
          />
          <Menu.Item
            name={intl.formatMessage({ id: "music" })}
            active={location.includes("videos/music")}
            onClick={() => navigate("/videos/music")}
          />
          <Menu.Item
            name={intl.formatMessage({ id: "company_ads" })}
            active={location.includes("videos/ads")}
            onClick={() => navigate("/videos/ads")}
          />
          <Menu.Item
            name={intl.formatMessage({ id: "events" })}
            active={location.includes("videos/events")}
            onClick={() => navigate("/videos/events")}
          />
          <Menu.Item
            name={intl.formatMessage({ id: "promotional" })}
            active={location.includes("videos/promo")}
            onClick={() => navigate("/videos/promo")}
          />
        </Menu>

        <Grid stackable columns={3} className="videoList">
          {posts.map(({ node }) => {
            return (
              <Grid.Column key={node.frontmatter.embed} className="noPadding">
                <VideoContainer
                  openModal={(e, src) => openModal(e, src)}
                  key={node.frontmatter.title}
                  slug={node.frontmatter.embed}
                  title={node.frontmatter.title}
                  photo={node.frontmatter.photo}
                />
              </Grid.Column>
            );
          })}
        </Grid>
      </div>
      <div id="myModal" style={{ display: "none" }} className="modal">
        <div
          className="close"
          onClick={() => {
            setEmbed("");
            document.getElementById("myModal").style.display = "none";
            document.getElementById("myModal").style.display = "none";
          }}
        >
          &times;
        </div>
        <div className="modal-content" id="modal-content" />
        <div className="vimeoMovieContainer fadeIn">
          {embed === "" ? (
            ""
          ) : (
            <iframe
              title={embed}
              id="vimeoMoviePlayed"
              className="vimeoMovie"
              src={`https://player.vimeo.com/video/${embed}?title=0&byline=0&portrait=0`}
              frameBorder="0"
              webkitallowfullscreen="true"
              mozallowfullscreen="true"
              allowFullScreen
            ></iframe>
          )}
        </div>
      </div>
    </Layout>
  );
};

export default injectIntl(Page);

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(
      sort: { fields: [frontmatter___order], order: ASC }
      filter: { fileAbsolutePath: { regex: "/(movies)/" } }
    ) {
      edges {
        node {
          excerpt
          fields {
            slug
          }
          frontmatter {
            date
            title
            embed
            photo {
              childImageSharp {
                # Specify a fluid image and fragment
                # The default maxWidth is 800 pixels
                fluid(quality: 95, maxWidth: 600) {
                  ...GatsbyImageSharpFluid_withWebp
                }
              }
            }
            category
          }
        }
      }
    }
  }
`;
