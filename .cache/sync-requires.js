const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => (m && m.default) || m


exports.components = {
  "component---cache-dev-404-page-js": hot(preferDefault(require("/Users/admin/Projects/Projects/obNew/.cache/dev-404-page.js"))),
  "component---src-pages-404-js": hot(preferDefault(require("/Users/admin/Projects/Projects/obNew/src/pages/404.js"))),
  "component---src-pages-about-js": hot(preferDefault(require("/Users/admin/Projects/Projects/obNew/src/pages/about.js"))),
  "component---src-pages-clients-js": hot(preferDefault(require("/Users/admin/Projects/Projects/obNew/src/pages/clients.js"))),
  "component---src-pages-contact-js": hot(preferDefault(require("/Users/admin/Projects/Projects/obNew/src/pages/contact.js"))),
  "component---src-pages-index-js": hot(preferDefault(require("/Users/admin/Projects/Projects/obNew/src/pages/index.js"))),
  "component---src-pages-photography-js": hot(preferDefault(require("/Users/admin/Projects/Projects/obNew/src/pages/photography.js"))),
  "component---src-pages-videos-ads-js": hot(preferDefault(require("/Users/admin/Projects/Projects/obNew/src/pages/videos/ads.js"))),
  "component---src-pages-videos-all-js": hot(preferDefault(require("/Users/admin/Projects/Projects/obNew/src/pages/videos/all.js"))),
  "component---src-pages-videos-events-js": hot(preferDefault(require("/Users/admin/Projects/Projects/obNew/src/pages/videos/events.js"))),
  "component---src-pages-videos-music-js": hot(preferDefault(require("/Users/admin/Projects/Projects/obNew/src/pages/videos/music.js"))),
  "component---src-pages-videos-promo-js": hot(preferDefault(require("/Users/admin/Projects/Projects/obNew/src/pages/videos/promo.js")))
}

