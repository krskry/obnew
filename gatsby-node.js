const path = require(`path`);
const { createFilePath } = require(`gatsby-source-filesystem`);
const { fmImagesToRelative } = require("gatsby-remark-relative-images");
const remark = require("remark");
const remarkHTML = require("remark-html");

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;

  const result = await graphql(
    `
      {
        allMarkdownRemark(
          sort: { fields: [frontmatter___date], order: DESC }
          limit: 1000
        ) {
          edges {
            node {
              fields {
                slug
              }
              frontmatter {
                locale
                category
                title
                embed
                link
                photo {
                  childImageSharp {
                    fluid {
                      base64
                      tracedSVG
                    }
                  }
                }
              }
            }
          }
        }
      }
    `
  );

  if (result.errors) {
    throw result.errors;
  }
};

const transformToHtml = (node, key) => {
  if (node.frontmatter && node.frontmatter[key]) {
    const markdown = node.frontmatter[key];
    node.frontmatter[key] = remark()
      .use(remarkHTML)
      .processSync(markdown)
      .toString();
  }
};

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { frontmatter } = node;
  transformToHtml(node, "contact_main");

  if (frontmatter) {
    const { photo } = frontmatter;
    if (photo) {
      if (photo.indexOf("/img") === 0) {
      }
    }
  }
  const { createNodeField } = actions;
  fmImagesToRelative(node);
  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode });
    createNodeField({
      name: `slug`,
      node,
      value,
    });
  }
};

exports.onCreatePage = ({ page, actions }) => {
  const { createPage, deletePage } = actions;
  deletePage(page);
  // You can access the variable "house" in your page queries now
  createPage({
    ...page,
    context: {
      ...page.context,
      locale: page.context.intl.language,
    },
  });
};
